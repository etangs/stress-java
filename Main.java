import java.util.stream.IntStream;
import java.lang.Math;
import java.util.Vector;
import java.lang.Thread;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
env variables:
- HIGH_MEM: memory used during stress period
- NOMINAL_MEM: momory used after stress period
- STRESS_INTERVAL: duration of stress period in second, negative values for constant stress
*/

public class Main {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        long creationTime = System.currentTimeMillis();
        int nominalMem = 200;
        long stressInterval=120L;
        long OneGigabytes = 1000000000L ;
        long OneMegabytes = 1000000L ;
        
        try {
            nominalMem = Integer.valueOf(System.getenv("NOMINAL_MEM"));
        } catch (Exception e) {
        };
        try {
            stressInterval = Long.valueOf(System.getenv("STRESS_INTERVAL"));
        } catch (Exception e) {
        };

        // Wait for stress period to end
        final long endOfStressTime = creationTime + stressInterval*1000;
        while ( Math.abs(System.currentTimeMillis() - endOfStressTime) > 100 ) {
            try{
                Thread.sleep(1);
            } catch (Exception e) {
            };
        }

        // Set nominal RAM
        System.out.println("Nominal mem: " + nominalMem);
        @SuppressWarnings("rawtypes")
        Vector v = new Vector();
        while ((nominalMem * OneMegabytes - Runtime.getRuntime().totalMemory() + Runtime.getRuntime().freeMemory()) > 1048576) {
            @SuppressWarnings("rawtypes")
            byte b1[] = new byte[104857600]; //100M
            v.add(b1);
            System.out.println("RAM: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/OneGigabytes);
        }

        // set readiness liveness probe
        try {
            FileWriter fileWriter = new FileWriter("/tmp/status");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.print("Ready");
            printWriter.close();
        } catch (Exception e) {
        };
        while (true) {
            try{
                Thread.sleep(5);
            } catch (Exception e) {
            };
        }
    }
}
