import java.util.stream.IntStream;
import java.lang.Math;
import java.util.Vector;
import java.lang.Thread;
/*
env variables:
- HIGH_MEM: memory used during stress period
- NOMINAL_MEM: momory used after stress period
- STRESS_INTERVAL: duration of stress period in second, negative values for constant stress
*/

public class Init {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        long creationTime = System.currentTimeMillis();
        int highMem = 2000;
        long stressInterval=120L;
        long OneMegabytes = 1000000L ;
        long OneGigabytes = 1000000000L ;

        
        try {
            highMem = Integer.valueOf(System.getenv("HIGH_MEM"));
        } catch (Exception e) {
        };
        try {
            stressInterval = Long.valueOf(System.getenv("STRESS_INTERVAL"));
        } catch (Exception e) {
        };

        // Stress RAM
        System.out.println("High mem: " + highMem);
        @SuppressWarnings("rawtypes")
        Vector v = new Vector();
        while ((highMem * OneMegabytes - Runtime.getRuntime().totalMemory() + Runtime.getRuntime().freeMemory()) > 1048576) {
            @SuppressWarnings("rawtypes")
            byte b1[] = new byte[104857600]; //100M
            v.add(b1);
            System.out.println("RAM: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/OneGigabytes);
        }
        
        // Stress CPU
        final long endOfStressTime = creationTime + stressInterval*1000;
        IntStream.range(0, 1000).parallel().forEach(i -> {
            while ( Math.abs(System.currentTimeMillis() - endOfStressTime) > 100 ) {
                // busy wait
            }
            System.out.println("Task " + i + " finished");
        });

        // set readiness liveness probe
        try {
            FileWriter fileWriter = new FileWriter("/tmp/status");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.print("Ready");
            printWriter.close();
        } catch (Exception e) {
        };
        
        // stay alive
        while (true) {
            try{
                Thread.sleep(5);
            } catch (Exception e) {
            };
        }


    }
}
