FROM openjdk
#FROM openjdk:8u252-jdk
COPY Main.java /app/Main.java
COPY Init.java /app/Init.java
#RUN javac Main.java
RUN javac Init.java
RUN echo "java -Xmx64G -Xms64G Init" > /app/start.sh
WORKDIR /app

CMD ["/bin/sh", "start.sh"]
